#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

typedef struct strutturaLista *tipoLista;

void Lista_Init(tipoLista *lista);
void Lista_Insert(tipoLista lista, void *dato);
void *Lista_Search(tipoLista lista, void *id, int (*Dato_Confronta)(void*, void*));
void Lista_Delete(tipoLista lista, void *id, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*));
void *Lista_PopFIFO(tipoLista lista);
void *Lista_PopLIFO(tipoLista lista);
void Lista_Ordina(tipoLista lista, int (*Dato_Confronta)(void*, void*));
void Lista_Free(tipoLista lista, void (*Dato_Free)(void*));
void Lista_Traverse(tipoLista lista, void (*Dato_Function)(void*));
void Lista_Print(tipoLista lista, FILE *fp, void (*Dato_Print)(void*, FILE*));
int Lista_GetSize(tipoLista lista);

void *Lista_GetHead(tipoLista lista);
void *Lista_GetCoda(tipoLista lista);
void *Lista_NextNodo(tipoLista lista, void *nodo);
void *Lista_PrevNodo(tipoLista lista, void *nodo);
void Lista_InsertBefore(tipoLista lista, void *nodo, void *dato);
void Lista_InsertAfter(tipoLista lista, void *nodo, void *dato);
void *Lista_NodoSearch(tipoLista lista, void* id, int (*Dato_Confronta)(void*, void*));
void Lista_NodoDelete(tipoLista lista, void *nodo, void (*Dato_Free)(void*));
void *Lista_GetDato(void *nodo);

#endif // LISTA_H_INCLUDED
