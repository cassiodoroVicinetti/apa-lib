#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

typedef struct strutturaNodo {
    void *dato;
    struct strutturaNodo *next, *prev;
} tipoNodo;

struct strutturaLista {
    tipoNodo *head, *coda;
    int N;
};

tipoNodo *DeleteNode(tipoLista lista, tipoNodo* current, void (*Dato_Free)(void*));
void *DeleteNode2(tipoLista lista, tipoNodo *current);
void Traverse(tipoNodo *current, void (*Dato_Function)(void*));
void Print(tipoNodo *current, FILE *fp, void (*Dato_Print)(void*, FILE*));
tipoNodo *Search(tipoLista lista, void* id, int (*Dato_Confronta)(void*, void*));

void Lista_Init(tipoLista *lista) {
    *lista = (tipoLista)malloc(sizeof(struct strutturaLista));
    (*lista)->head = NULL;
    (*lista)->coda = NULL;
    (*lista)->N = 0;
}

void Lista_Insert(tipoLista lista, void *dato) {
    if (lista->head == NULL) {
        lista->head = (tipoNodo*)malloc(sizeof(tipoNodo));
        lista->head->dato = dato;
        lista->head->prev = NULL;
        lista->head->next = NULL;
        lista->coda = lista->head;

    } else { // Inserimento in coda
        lista->coda->next = (tipoNodo*)malloc(sizeof(tipoNodo));
        lista->coda->next->prev = lista->coda;
        lista->coda = lista->coda->next;
        lista->coda->next = NULL;
        lista->coda->dato = dato;
    }
    lista->N++;
}

void *Lista_Search(tipoLista lista, void *id, int (*Dato_Confronta)(void*, void*)) {
    tipoNodo *current;

    current = Search(lista, id, Dato_Confronta);
    if (current != NULL)
        return (current->dato);
    else
        return NULL;
}

void Lista_Delete(tipoLista lista, void *id, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*)) {
    tipoNodo *current;

    if ((current = Search(lista, id, Dato_Confronta)) != NULL)
        DeleteNode(lista, current, Dato_Free);
}

void *Lista_PopLIFO(tipoLista lista) {
	if (lista->N == 0)
		return NULL;
	return DeleteNode2(lista, lista->coda);
}

void *Lista_PopFIFO(tipoLista lista) {
	if (lista->N == 0)
		return NULL;
	return DeleteNode2(lista, lista->head);
}

void Lista_Ordina(tipoLista lista, int (*Dato_Confronta)(void*, void*)) {
    // Insertion sort
    struct strutturaNodo *corr, *corr2;
    void *tmp;

    if (lista->N == 0)
        return;
    corr = lista->head->next;
    while (corr != NULL) {
        corr2 = corr->prev;
        while (corr2 != NULL && (*Dato_Confronta)(corr2->dato, corr2->next->dato) > 0) {
            tmp = corr2->dato;
            corr2->dato = corr2->next->dato;
            corr2->next->dato = tmp;

            corr2 = corr2->prev;
        }
        corr = corr->next;
    }
}

void Lista_Free(tipoLista lista, void (*Dato_Free)(void*)) {
    tipoNodo *current;
	current = lista->head;
	while (current != NULL) {
		current = DeleteNode(lista, current, Dato_Free);
	}
	free(lista);
}

void Lista_Traverse(tipoLista lista, void (*Dato_Function)(void*)) {
    Traverse(lista->head, Dato_Function);
}

void Lista_Print(tipoLista lista, FILE *fp, void (*Dato_Print)(void*, FILE*)) {
	Print(lista->head, fp, Dato_Print);
}

int Lista_GetSize(tipoLista lista) {
    return lista->N;
}

void *Lista_GetHead(tipoLista lista) {
	return lista->head;
}

void *Lista_GetCoda(tipoLista lista) {
	return lista->coda;
}

void *Lista_NextNodo(tipoLista lista, void *nodo) {
	return (((tipoNodo*)nodo)->next);
}

void *Lista_PrevNodo(tipoLista lista, void *nodo) {
	return (((tipoNodo*)nodo)->prev);
}

void Lista_InsertBefore(tipoLista lista, void *nodo, void *dato) {
  tipoNodo *current;
  tipoNodo *prev;
  
  current = (tipoNodo*)nodo;
  if (current->prev == NULL) { // Head
    lista->head->prev = (tipoNodo*)malloc(sizeof(tipoNodo));
    lista->head->prev->next = lista->head;
    lista->head = lista->head->prev;
    lista->head->prev = NULL;
    lista->head->dato = dato;
  } else {
    prev = (tipoNodo*)current->prev;
    current->prev = (tipoNodo*)malloc(sizeof(tipoNodo));
    current->prev->next = current;
    current->prev->dato = dato;
    current->prev->prev = prev;
    prev->next = current->prev;
  }
  
  lista->N++;
}

void Lista_InsertAfter(tipoLista lista, void *nodo, void *dato) {
  tipoNodo *current;
  tipoNodo *next;
  
  current = (tipoNodo*)nodo;
  if (current->next == NULL) { // Coda
    Lista_Insert(lista, dato);
    return;
  }
  
  next = (tipoNodo*)current->next;
  
  current->next = (tipoNodo*)malloc(sizeof(tipoNodo));
  current->next->prev = current;
  current->next->dato = dato;
  current->next->next = next;
  next->prev = current->next;
  
  lista->N++;
}

void *Lista_NodoSearch(tipoLista lista, void* id, int (*Dato_Confronta)(void*, void*)) {
    tipoNodo *current;

    current = Search(lista, id, Dato_Confronta);
    if (current != NULL)
        return ((void*)current);
    else
        return NULL;
}

void Lista_NodoDelete(tipoLista lista, void *nodo, void (*Dato_Free)(void*)) {
  DeleteNode(lista, (tipoNodo*)nodo, Dato_Free);
}

void *Lista_GetDato(void *nodo) {
    return (((tipoNodo*)nodo)->dato);
}

tipoNodo *DeleteNode(tipoLista lista, tipoNodo *current, void (*Dato_Free)(void*)) {
    tipoNodo *reply;
    void *dato;
    reply = current->next;

    dato = DeleteNode2(lista, current);
    if (Dato_Free != NULL)
        (*Dato_Free)(dato);
    return reply;
}

void *DeleteNode2(tipoLista lista, tipoNodo *current) {
	void *reply;
	reply = current->dato;

    if (current->prev == NULL) {
        lista->head = current->next;
        if (lista->head != NULL)
            lista->head->prev = NULL;
        else
            lista->coda = NULL;

    } else {
        current->prev->next = current->next;
        if (current->next == NULL)
            lista->coda = current->prev;
	else
	  current->next->prev = current->prev;
    }

    free(current);
    lista->N--;
	return reply;
}

void Traverse(tipoNodo *current, void (*Dato_Function)(void*)) {
    if (current == NULL)
        return;
    (*Dato_Function)(current->dato);
    Traverse(current->next, Dato_Function);
}

void Print(tipoNodo *current, FILE *fp, void (*Dato_Print)(void*, FILE*)) {
	if (current == NULL)
		return;
	(*Dato_Print)(current->dato, fp);
	Print(current->next, fp, Dato_Print);
}

tipoNodo *Search(tipoLista lista, void* id, int (*Dato_Confronta)(void*, void*)) {
    tipoNodo *current;

    current = lista->head;
    while (current != NULL) {
        if ((*Dato_Confronta)(current->dato, id) == 0)
            return current;
        current = current->next;
    }
    return NULL;
}
