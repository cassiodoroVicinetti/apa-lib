#include <stdio.h>
#include <stdlib.h>
#include "hash.h"

#define M 97

struct strutturaHash {
	tipoLista *liste;
	int N;
};

int PrivateGetHash(void *dato, int modo, void *(*Dato_GetKey)(void*));
int PrivateGetHash2(void *k, int modo);
int hashU(char *v);

void Hash_Init(tipoHash *hash) {
	int i;
	*hash = (tipoHash)malloc(sizeof(tipoHash*));
	(*hash)->liste = (tipoLista*)malloc(sizeof(tipoLista) * M);
	for (i = 0; i < M; i++)
		(*hash)->liste[i] = NULL;
	(*hash)->N = 0;
}

void Hash_Free(tipoHash hash, void (*Dato_Free)(void*)) {
	int i;
	for (i = 0; i < M; i++)
		if (hash->liste[i] != NULL)
			Lista_Free(hash->liste[i], Dato_Free);
	free(hash);
}

void Hash_Insert(tipoHash hash, void *dato, int modo, void *(*Dato_GetKey)(void*)) {
	int h;
	h = PrivateGetHash(dato, modo, Dato_GetKey);
	if (hash->liste[h] == NULL)
		Lista_Init(&(hash->liste[h]));
	Lista_Insert(hash->liste[h], dato);
	hash->N++;
}

int PrivateGetHash(void *dato, int modo, void *(*Dato_GetKey)(void*)) {
	void *k;

	k = (*Dato_GetKey)(dato);
	return PrivateGetHash2(k, modo);
}

int PrivateGetHash2(void *k, int modo) {
	const int l = 0;
	const int r = 1;
	switch (modo) {
		case Hash_int:
			return *((int*)k) % M;
		case Hash_float: // Tra 0 e 1
		 	return (int)(M * ((*((float*)k) - l) / (r - l))) + 1;
		case Hash_string:
			return hashU(*((char**)k));
	}
}

int hashU(char *v) {
  int h, a = 31415;
	const int b = 27183;

  for ( h = 0; *v != '\0'; v++, a = a*b % (M-1))
        h = (a*h + *v) % M;

  return h;
}

void Hash_Delete(tipoHash hash, void *k, int modo, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*)) {
	int h;
	h = PrivateGetHash2(k, modo);
	if (hash->liste[h] == NULL)
		return;
	Lista_Delete(hash->liste[h], k, Dato_Confronta, Dato_Free);
	hash->N--;
}

int Hash_GetSize(tipoHash hash) {
	return hash->N;
}

void *Hash_Search(tipoHash hash, void *k, int modo, int (*Dato_Confronta)(void*, void*)) {
	int h;
	h = PrivateGetHash2(k, modo);
	if (hash->liste[h] == NULL)
		return NULL;
	return Lista_Search(hash->liste[h], k, Dato_Confronta);
}

void Hash_Traverse(tipoHash hash, void (*Dato_Function)(void*)) {
	int i;
	for (i = 0; i < M; i++)
		if (hash->liste[i] != NULL)
			Lista_Traverse(hash->liste[i], Dato_Function);
}

void Hash_Print(tipoHash hash, FILE *fp, void (*Dato_Print)(void*, FILE*)) {
	int i;
	for (i = 0; i < M; i++)
		if (hash->liste[i] != NULL)
			Lista_Print(hash->liste[i], fp, Dato_Print);
}
