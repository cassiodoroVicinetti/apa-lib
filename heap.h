#ifndef HEAP_H_INCLUDED
#define HEAP_H_INCLUDED

typedef struct strutturaHeap *tipoHeap;

void Heap_Init(tipoHeap *heap, int maxDim);
void Heap_Free(tipoHeap heap, void (*Dato_Free)(void*));
int Heap_Insert(tipoHeap heap, void *dato, int (*Dato_Confronta)(void*, void*));
void *Heap_ExtractMax(tipoHeap heap, int (*Dato_Confronta)(void*, void*));
void *Heap_Maximum(tipoHeap heap);
void Heap_Traverse(tipoHeap heap, void (*Dato_Function)(void*));
void Heap_Print(tipoHeap heap, FILE *fp, void (*Dato_Print)(void*, FILE*));
void Heap_Build(tipoHeap heap, int (*Dato_Confronta)(void*, void*));
void Heap_Sort(void **array, int size, int (*Dato_Confronta)(void*, void*));
int Heap_GetSize(tipoHeap heap);

#endif // HEAP_H_INCLUDED
