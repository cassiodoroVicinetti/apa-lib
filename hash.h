#ifndef _HASH_INCLUDED
#define _HASH_INCLUDED

#include "lista.h"
#define Hash_int   -1
#define Hash_float     0
#define Hash_string   1

typedef struct strutturaHash *tipoHash;

void Hash_Init(tipoHash *hash);
void Hash_Free(tipoHash hash, void (*Dato_Free)(void*));
void Hash_Insert(tipoHash hash, void *dato, int modo, void *(*Dato_GetKey)(void*));
void Hash_Delete(tipoHash hash, void *k, int modo, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*));
int Hash_GetSize(tipoHash hash);
void *Hash_Search(tipoHash hash, void *k, int modo, int (*Dato_Confronta)(void*, void*));
void Hash_Traverse(tipoHash hash, void (*Dato_Function)(void*));
void Hash_Print(tipoHash hash, FILE *fp, void (*Dato_Print)(void*, FILE*));

#endif
