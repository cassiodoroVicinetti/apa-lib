#include <stdio.h>
#include <stdlib.h>
#include "heap.h"

#define LEFT(i)   	((i*2) + 1)
#define RIGHT(i)	((i*2) + 2)
#define PARENT(i)	((i-1) / 2)

struct strutturaHeap {
    void **nodi;
    int dim, maxDim;
};

void Heapify(tipoHeap heap, int i, int (*Dato_Confronta)(void*, void*));
void FreeNodo(void *nodo, void (*Dato_Free)(void*));
void Swap(void **nodi, int i, int largest);
void PrivateBuildHeap(void **array, int size; int (*Dato_Confronta)(void*, void*));
void PrivateHeapify(void **array, int size, int i, int (*Dato_Confronta)(void*, void*));

void Heap_Init(tipoHeap *heap, int maxDim) {
    *heap = (tipoHeap)malloc(sizeof(struct strutturaHeap));
    (*heap)->nodi = (void**)malloc(sizeof(void*) * maxDim);
    (*heap)->dim = 0;
    (*heap)->maxDim = maxDim;
}

void Heap_Free(tipoHeap heap, void (*Dato_Free)(void*)) {
    int i;
    if (heap->dim > 0)
        for (i = 0; i < heap->dim; i++)
            FreeNodo(heap->nodi[i], Dato_Free);

    free(heap->nodi);
    free(heap);
}

void FreeNodo(void *nodo, void (*Dato_Free)(void*)) {
    if (Dato_Free != NULL)
		(*Dato_Free)(nodo);
}

int Heap_Insert(tipoHeap heap, void *dato, int (*Dato_Confronta)(void*, void*)) {
	int i;

    if (heap->dim == heap->maxDim)
        return 0;

 	i = heap->dim;
	while (i > 0 && (*Dato_Confronta)(heap->nodi[PARENT(i)], dato) < 0) {
        heap->nodi[i] = heap->nodi[PARENT(i)];
        i = PARENT(i);
    }

	heap->nodi[i] = dato;
	heap->dim++;
	return 1;
}

void *Heap_ExtractMax(tipoHeap heap, int (*Dato_Confronta)(void*, void*)) {
    void *deposito;

    if (heap->dim == 0)
        return NULL;

    deposito = Heap_Maximum(heap);
    heap->dim--;
    heap->nodi[0] = heap->nodi[heap->dim];
    Heapify(heap, 0, Dato_Confronta);
    return deposito;
}

void *Heap_Maximum(tipoHeap heap) {
    if (heap->dim == 0)
        return NULL;
    return heap->nodi[0];
}

void Heap_Traverse(tipoHeap heap, void (*Dato_Function)(void*)) {
    int i;
    if (heap->dim == 0)
        return;
    for (i = 0; i < heap->dim; i++)
        (*Dato_Function)(heap->nodi[i]);
}

void Heap_Print(tipoHeap heap, FILE *fp, void (*Dato_Print)(void*, FILE*)) {
    int i;
    if (heap->dim == 0)
        return;
    for (i = 0; i < heap->dim; i++)
        (*Dato_Print)(heap->nodi[i], fp);
}

void Heap_Build(tipoHeap heap, int (*Dato_Confronta)(void*, void*)) {
	PrivateBuildHeap(heap->nodi, heap->dim, Dato_Confronta);
}

void PrivateBuildHeap(void **array, int size; int (*Dato_Confronta)(void*, void*)) {
	int i;
	for (i = PARENT(size); i >= 0; i--)
		PrivateHeapify(array, size, i, Dato_Confronta);
}

void Heapify(tipoHeap heap, int i, int (*Dato_Confronta)(void*, void*)) {
	PrivateHeapify(heap->nodi, heap->dim, i, Dato_Confronta);
}

void PrivateHeapify(void **array, int size, int i, int (*Dato_Confronta)(void*, void*)) {
    int l, r, largest;

    l = LEFT(i);
    r = RIGHT(i);
    largest = i;

    if (l < size && (*Dato_Confronta)(array[l], array[largest]) > 0)
        largest = l;

    if (r < size && (*Dato_Confronta)(array[r], array[largest]) > 0)
        largest = r;

    if (largest != i) {
        Swap(array, i, largest);
        PrivateHeapify(array, size, largest, Dato_Confronta);
    }
}

void Heap_Sort(void **array, int size, int (*Dato_Confronta)(void*, void*)) {
  int i;

  PrivateBuildHeap(array, size, Dato_Confronta);
  for (i=size-1; i>0; i--) {
    Swap(array, i, 0);
    PrivateHeapify(array, --size, 0, Dato_Confronta);
  }
}

void Swap(void **nodi, int i, int largest) {
	void *deposito;
	deposito = nodi[i];
	nodi[i] = nodi[largest];
	nodi[largest] = deposito;
}

int Heap_GetSize(tipoHeap heap) {
    return heap->dim;
}
