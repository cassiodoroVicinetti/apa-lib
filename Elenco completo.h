﻿// bst.h
#define BST_preorder   -1
#define BST_inorder     0
#define BST_postorder   1

typedef struct strutturaBST *tipoBST;

void BST_Init(tipoBST *bst);
void BST_Free(tipoBST bst, void (*Dato_Free)(void*));
int BST_Insert(tipoBST *bst, void *dato, int (*Dato_Confronta)(void*, void*));
void *BST_Search(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*));
void BST_SearchAll(tipoBST bst, void *id, int *occurencesIndexes, void **occurrencesData, int *N, int (*Dato_Confronta)(void*, void*));
void *BST_Select(tipoBST bst, int k);
void BST_Delete(tipoBST *bst, int k, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*));
void *BST_GetMin(tipoBST bst);
void *BST_GetMax(tipoBST bst);
void *BST_GetSuccessor(tipoBST bst, int k);
void *BST_GetPrecedessor(tipoBST bst, int k);
void BST_Visit(tipoBST bst, int modo, void (*Dato_Function)(void*));
void BST_Print(tipoBST bst, int modo, FILE *fp, void (*Dato_Print)(void*, FILE*));
int BST_GetSize(tipoBST bst);

void BST_RootInsert(tipoBST *bst, void *dato, int (*Dato_Confronta)(void*, void*));
void BST_Partition(tipoBST bst, int k, int (*Dato_Confronta)(void*, void*));


// hash.h
#include "lista.h"
#define Hash_int   -1
#define Hash_float     0
#define Hash_string   1

typedef struct strutturaHash *tipoHash;

void Hash_Init(tipoHash *hash);
void Hash_Free(tipoHash hash, void (*Dato_Free)(void*));
void Hash_Insert(tipoHash hash, void *dato, int modo, void *(*Dato_GetKey)(void*));
void Hash_Delete(tipoHash hash, void *k, int modo, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*));
int Hash_GetSize(tipoHash hash);
void *Hash_Search(tipoHash hash, void *k, int modo, int (*Dato_Confronta)(void*, void*));
void Hash_Traverse(tipoHash hash, void (*Dato_Function)(void*));
void Hash_Print(tipoHash hash, FILE *fp, void (*Dato_Print)(void*, FILE*));


// heap.h
typedef struct strutturaHeap *tipoHeap;

void Heap_Init(tipoHeap *heap, int maxDim);
void Heap_Free(tipoHeap heap, void (*Dato_Free)(void*));
int Heap_Insert(tipoHeap heap, void *dato, int (*Dato_Confronta)(void*, void*));
void *Heap_ExtractMax(tipoHeap heap, int (*Dato_Confronta)(void*, void*));
void *Heap_Maximum(tipoHeap heap);
void Heap_Traverse(tipoHeap heap, void (*Dato_Function)(void*));
void Heap_Print(tipoHeap heap, FILE *fp, void (*Dato_Print)(void*, FILE*));
void Heap_Build(tipoHeap heap, int (*Dato_Confronta)(void*, void*));
void Heap_Sort(void **array, int size, int (*Dato_Confronta)(void*, void*));
int Heap_GetSize(tipoHeap heap);


// lista.h
typedef struct strutturaLista *tipoLista;

void Lista_Init(tipoLista *lista);
void Lista_Insert(tipoLista lista, void *dato);
void *Lista_Search(tipoLista lista, void *id, int (*Dato_Confronta)(void*, void*));
void Lista_Delete(tipoLista lista, void *id, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*));
void *Lista_PopFIFO(tipoLista lista);
void *Lista_PopLIFO(tipoLista lista);
void Lista_Ordina(tipoLista lista, int (*Dato_Confronta)(void*, void*));
void Lista_Free(tipoLista lista, void (*Dato_Free)(void*));
void Lista_Traverse(tipoLista lista, void (*Dato_Function)(void*));
void Lista_Print(tipoLista lista, FILE *fp, void (*Dato_Print)(void*, FILE*));
int Lista_GetSize(tipoLista lista);

void *Lista_GetHead(tipoLista lista);
void *Lista_GetCoda(tipoLista lista);
void *Lista_NextNodo(tipoLista lista, void *nodo);
void *Lista_PrevNodo(tipoLista lista, void *nodo);
void Lista_InsertBefore(tipoLista lista, void *nodo, void *dato);
void Lista_InsertAfter(tipoLista lista, void *nodo, void *dato);
void *Lista_NodoSearch(tipoLista lista, void* id, int (*Dato_Confronta)(void*, void*));
void Lista_NodoDelete(tipoLista lista, void *nodo, void (*Dato_Free)(void*));
void *Lista_GetDato(void *nodo);


// vett.h
void Vett_InsertionSort(void **A, int n, int (*Dato_Confronta)(void*, void*));
void *Vett_BinSearch(void **v, void *k, int N, int (*Dato_Confronta)(void*, void*));
void *Vett_GetMax(void **a, int N, int (*Dato_Confronta)(void*, void*));


// _int.h
int Int_Confronta(int *num1, int *num2);
int Int_GetKey(int *num);
void Int_Print(int *num);


// _float.h
int Float_Confronta(float *num1, float *num2);
float Float_GetKey(float *num);
void Float_Print(float *num);


// _str.h
int Str_Confronta(char **str1, char **str2);
char *Str_GetKey(char **str);
void Str_Print(char **str);
void Str_Free(char **str);