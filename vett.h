#ifndef _VETT_INCLUDED
#define _VETT_INCLUDED

void Vett_InsertionSort(void **A, int n, int (*Dato_Confronta)(void*, void*));
void *Vett_BinSearch(void **v, void *k, int N, int (*Dato_Confronta)(void*, void*));
void *Vett_GetMax(void **a, int N, int (*Dato_Confronta)(void*, void*));

#endif
