#include <stdio.h>
#include <stdlib.h>
#include "vett.h"

void *PrivateBinSearch(void **v, void *k, int a, int b, int (*Dato_Confronta)(void*, void*));
void *PrivateGetMax(void **a, int l, int r, int (*Dato_Confronta)(void*, void*));

void Vett_InsertionSort(void **A, int n, int (*Dato_Confronta)(void*, void*)) {
	int i, j;
	void *x;
	for(i=1; i<n; i++)
	{
		x = A[i];
		j = i - 1;
		while (j >= 0 && (*Dato_Confronta)(x, A[j]) < 0)
		{
			A[j+1] = A[j];
			j--;
		}
		A[j+1] = x;
	}
}

void *Vett_BinSearch(void **v, void *k, int N, int (*Dato_Confronta)(void*, void*)) {
	return PrivateBinSearch(v, k, 0, N - 1, Dato_Confronta);
}

void *PrivateBinSearch(void **v, void *k, int a, int b, int (*Dato_Confronta)(void*, void*)) {
	int c;
	if((b-a) == 0)
		if((*Dato_Confronta)(v[a], k) == 0) return(v[a]);
		else return(NULL);
	c = (a+b) / 2;
	if((*Dato_Confronta)(v[c], k) >= 0)
		return(PrivateBinSearch(v, k, a, c, Dato_Confronta));
	else return(PrivateBinSearch(v, k, c+1, b, Dato_Confronta));
}

void *Vett_GetMax(void **a, int N, int (*Dato_Confronta)(void*, void*)) {
	return PrivateGetMax(a, 0, N - 1, Dato_Confronta);
}

void *PrivateGetMax(void **a, int l, int r, int (*Dato_Confronta)(void*, void*)) {
   void *u, *v ;
   int m = (l + r)/2 ;
   if (l == r) return a[l] ;
   u = PrivateGetMax (a, l, m, Dato_Confronta);
   v = PrivateGetMax (a, m+1, r, Dato_Confronta);
   if (Dato_Confronta(u, v) > 0) return u;
     else return v;
}
