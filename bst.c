#include <stdio.h>
#include <stdlib.h>
#include "bst.h"

struct strutturaBST {
	void *dato;
	tipoBST left, right, padre;
	int t;
};

tipoBST PrivateInsert(tipoBST *bst, void *dato, int *k, int (*Dato_Confronta)(void*, void*));
tipoBST PrivateInsert2(tipoBST *bst, void *dato, int *k, tipoBST *padre, int (*Dato_Confronta)(void*, void*));
tipoBST PrivateSearch(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*));
tipoBST PrivateSearch2(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*));
tipoBST PrivateGetMax(tipoBST bst);
tipoBST PrivateSelect(tipoBST bst, int k, int decr);
void Rotate(tipoBST bst, int k_x, int left, int (*Dato_Confronta)(void*, void*));
void PrivateRotate(tipoBST x, int left, int (*Dato_Confronta)(void*, void*));
void PrivatePartition(tipoBST node, tipoBST padre_root, int (*Dato_Confronta)(void*, void*));

void BST_Init(tipoBST *bst) {
	*bst = NULL;
}

void BST_Free(tipoBST bst, void (*Dato_Free)(void*)) {
	if (bst == NULL)
		return;
	BST_Free(bst->left, Dato_Free);
	BST_Free(bst->right, Dato_Free);
	if (Dato_Free != NULL)
		(*Dato_Free)(bst->dato);
	free(bst);
}

int BST_Insert(tipoBST *bst, void *dato, int (*Dato_Confronta)(void*, void*)) {
	int k;
	PrivateInsert(bst, dato, &k, Dato_Confronta);
	return k;
}

void *BST_Search(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*)) {
	tipoBST node;
	node = PrivateSearch(bst, id, k, Dato_Confronta);
	if (node == NULL)
		return NULL;
	return node->dato;
}

void BST_SearchAll(tipoBST bst, void *id, int *k, void **reply, int *N, int (*Dato_Confronta)(void*, void*)) {
    int i, dim;
    tipoBST key;

    dim = BST_GetSize(bst);
    *N = 0;
    if (dim == 0)
        return NULL;

    i = 0;
    key = PrivateSearch(bst, id, &(k[i]), Dato_Confronta);
    while (i < dim && key != NULL) {
        *N += 1;
        reply[i] = key->dato;
        i++;
        key = PrivateSearch(key->left, id, &(k[i]), Dato_Confronta);
    }
}

void *BST_Select(tipoBST bst, int k) {
	tipoBST node;
	node = PrivateSelect(bst, k, 0);
	if (node == NULL)
		return NULL;
	return node->dato;
}

void BST_Delete(tipoBST *bst, int k, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*)) {
	tipoBST node, child;

	node = PrivateSelect(*bst, k, 1);
	if (node == NULL)
		return;

	if (Dato_Free != NULL)
		(*Dato_Free)(node->dato);

	if (node->left != NULL) {
		if (node->right != NULL) {
			child = PrivateGetMax(node->left); // Predecessore
			PrivatePartition(child, node, Dato_Confronta);
			child->right = node->right;
			node->right->padre = child;

		} else // node->right == NULL
			child = node->left;
	} else if (node->right != NULL) // && node->left == NULL
		child = node->right;
	else // Foglia: node->right == NULL && node->left == NULL
		child = NULL;

	if (node->padre != NULL) {
		if ((*Dato_Confronta)(node->dato, node->padre->dato) > 0)
			node->padre->right = child;
		else
			node->padre->left = child;
	} else
		*bst = child;
	free(node);
}

void *BST_GetMin(tipoBST bst) {
	void *dato;
	if (bst == NULL)
		return NULL;

	dato = BST_GetMin(bst->left);
	if (dato == NULL)
		return bst->dato;
	return dato;
}

void *BST_GetMax(tipoBST bst) {
	tipoBST node;
	node = PrivateGetMax(bst);
	if (node == NULL)
		return NULL;
	return node->dato;
}

void *BST_GetSuccessor(tipoBST bst, int k) {
	return BST_Select(bst, k + 1);
}

void *BST_GetPrecedessor(tipoBST bst, int k) {
	return BST_Select(bst, k - 1);
}

void BST_Visit(tipoBST bst, int modo, void (*Dato_Function)(void*)) {
	if (bst == NULL)
		return;
	if (modo == BST_preorder)
		Dato_Function(bst->dato);
	BST_Visit(bst->left, modo, Dato_Function);
	if (modo == BST_inorder)
		Dato_Function(bst->dato);
	BST_Visit(bst->right, modo, Dato_Function);
	if (modo == BST_postorder)
		Dato_Function(bst->dato);
}

void BST_Print(tipoBST bst, int modo, FILE *fp, void (*Dato_Print)(void*, FILE*)) {
	if (bst == NULL)
		return;
	if (modo == BST_preorder)
		Dato_Print(bst->dato, fp);
	BST_Print(bst->left, modo, fp, Dato_Print);
	if (modo == BST_inorder)
		Dato_Print(bst->dato, fp);
	BST_Print(bst->right, modo, fp, Dato_Print);
	if (modo == BST_postorder)
		Dato_Print(bst->dato, fp);
}

int BST_GetSize(tipoBST bst) {
	return bst->t;
}

void BST_RootInsert(tipoBST *bst, void *dato, int (*Dato_Confronta)(void*, void*)) {
	int k;
	tipoBST node;
	node = PrivateInsert(bst, dato, &k, Dato_Confronta);
	PrivatePartition(node, NULL, Dato_Confronta);
}

void BST_Partition(tipoBST bst, int k, int (*Dato_Confronta)(void*, void*)) {
	tipoBST node;
	node = PrivateSelect(bst, k, 0);
	if (node != NULL)
		PrivatePartition(node, NULL, Dato_Confronta);
}

tipoBST PrivateInsert(tipoBST *bst, void *dato, int *k, int (*Dato_Confronta)(void*, void*)) {
	*k = -1;
	return PrivateInsert2(bst, dato, k, NULL, Dato_Confronta);
}

tipoBST PrivateInsert2(tipoBST *bst, void *dato, int *k, tipoBST *padre, int (*Dato_Confronta)(void*, void*)) {
	if (*bst == NULL) {
		*bst = (tipoBST)malloc(sizeof(struct strutturaBST));
		(*bst)->dato = dato;
		(*bst)->left = NULL;
		(*bst)->right = NULL;
		(*bst)->t = 1;
		if (padre != NULL)
            (*bst)->padre = *padre;
        else
            (*bst)->padre = NULL;
		*k += 1;
		return *bst;
	}

	(*bst)->t++;
	if ((*Dato_Confronta)(dato, (*bst)->dato) <= 0)
		return PrivateInsert2(&((*bst)->left), dato, k, bst, Dato_Confronta);

	*k += 1;
	if ((*bst)->left != NULL)
		*k += (*bst)->left->t;
	return PrivateInsert2(&((*bst)->right), dato, k, bst, Dato_Confronta);
}

tipoBST PrivateSearch(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*)) {
	*k = 0;
	return PrivateSearch2(bst, id, k, Dato_Confronta);
}

tipoBST PrivateSearch2(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*)) {
	int result;
	if (bst == NULL) {
		*k = -1;
		return NULL;
	}

	result = (*Dato_Confronta)(id, bst->dato);
	if (result < 0)
		return PrivateSearch2(bst->left, id, k, Dato_Confronta);

	if (bst->left != NULL)
		*k = *k + bst->left->t;
	if (result == 0)
		return bst;

	*k = *k + 1;
	// if (result > 0)
		return PrivateSearch2(bst->right, id, k, Dato_Confronta);
}

tipoBST PrivateGetMax(tipoBST bst) {
	tipoBST node;
	if (bst == NULL)
		return NULL;
	node = PrivateGetMax(bst->right);
	if (node == NULL)
		return bst;
	return node;
}

tipoBST PrivateSelect(tipoBST bst, int k, int decr) {
	int t;
	if (bst == NULL)
		return NULL;

	if (bst->left == NULL)
		t = 0;
	else
		t = bst->left->t;

	if (k == t)
		return bst;

    if (decr == 1) // Flag richiesto da Delete()
        bst->t -= 1;

	if (k < t)
		return PrivateSelect(bst->left, k, decr);
	// if (k > t)
		return PrivateSelect(bst->right, k - t - 1, decr);
}

void Rotate(tipoBST bst, int k_x, int left, int (*Dato_Confronta)(void*, void*)) {
	tipoBST x;
	x = PrivateSelect(bst, k_x, 0);
	if (x != NULL && x->padre != NULL)
		PrivateRotate(x, left, Dato_Confronta);
}

void PrivateRotate(tipoBST x, int left, int (*Dato_Confronta)(void*, void*)) {
	if (x->padre->padre != NULL) {
		if ((*Dato_Confronta)(x->padre->dato, x->padre->padre->dato) > 0)
			x->padre->padre->right = x;
		else
			x->padre->padre->left = x;
	}

	x->t++;
	x->padre->t--;

	if (left = 1) { // RotateLeft
		if (x->padre->left != NULL)
			x->t += x->padre->left->t;
		if (x->right != NULL)
			x->padre->t -= x->right->t;

		if (x->left != NULL)
			x->left->padre = x->padre;
		x->padre->right = x->left;
		x->left = x->padre;
		x->padre = x->left->padre;
		x->left->padre = x;

	} else { // RotateRight
		if (x->padre->right != NULL)
			x->t += x->padre->right->t;
		if (x->left != NULL)
			x->padre->t -= x->left->t;

		if (x->right != NULL)
			x->right->padre = x->padre;
		x->padre->left = x->right;
		x->right = x->padre;
		x->padre = x->right->padre;
		x->right->padre = x;
	}
}

void PrivatePartition(tipoBST node, tipoBST padre_root, int (*Dato_Confronta)(void*, void*)) {
	while (node->padre != padre_root) {
		PrivateRotate(node, ((*Dato_Confronta)(node->dato, node->padre->dato) > 0) ? 1 : 0, Dato_Confronta);
	}
}
