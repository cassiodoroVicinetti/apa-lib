#ifndef _BST_INCLUDED
#define _BST_INCLUDED

#define BST_preorder   -1
#define BST_inorder     0
#define BST_postorder   1

typedef struct strutturaBST *tipoBST;

void BST_Init(tipoBST *bst);
void BST_Free(tipoBST bst, void (*Dato_Free)(void*));
int BST_Insert(tipoBST *bst, void *dato, int (*Dato_Confronta)(void*, void*));
void *BST_Search(tipoBST bst, void *id, int *k, int (*Dato_Confronta)(void*, void*));
void BST_SearchAll(tipoBST bst, void *id, int *occurrencesIndexes, void **occurrencesData, int *N, int (*Dato_Confronta)(void*, void*));
void *BST_Select(tipoBST bst, int k);
void BST_Delete(tipoBST *bst, int k, int (*Dato_Confronta)(void*, void*), void (*Dato_Free)(void*));
void *BST_GetMin(tipoBST bst);
void *BST_GetMax(tipoBST bst);
void *BST_GetSuccessor(tipoBST bst, int k);
void *BST_GetPrecedessor(tipoBST bst, int k);
void BST_Visit(tipoBST bst, int modo, void (*Dato_Function)(void*));
void BST_Print(tipoBST bst, int modo, FILE *fp, void (*Dato_Print)(void*, FILE*));
int BST_GetSize(tipoBST bst);

void BST_RootInsert(tipoBST *bst, void *dato, int (*Dato_Confronta)(void*, void*));
void BST_Partition(tipoBST bst, int k, int (*Dato_Confronta)(void*, void*));

#endif
