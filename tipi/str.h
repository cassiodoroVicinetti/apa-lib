#ifndef _STR_INCLUDED
#define _STR_INCLUDED

int Str_Confronta(char **str1, char **str2);
char *Str_GetKey(char **str);
void Str_Print(char **str);
void Str_Free(char **str);

#endif
