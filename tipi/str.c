#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "str.h"

int String_Confronta(char **str1, char **str2) {
	return strcmp(*str1, *str2);
}

char *String_GetKey(char **str) {
	return *str;
}

void Str_Print(char **str) {
	printf("%s\n", *str);
}

void Str_Free(char **str) {
	free(*str);
}