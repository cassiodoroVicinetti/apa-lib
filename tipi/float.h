#ifndef _FLOAT_INCLUDED
#define _FLOAT_INCLUDED

int Float_Confronta(float *num1, float *num2);
float Float_GetKey(float *num);
void Float_Print(float *num);

#endif
