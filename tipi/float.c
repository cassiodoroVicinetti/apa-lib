#include <stdio.h>
#include <stdlib.h>
#include "float.h"

int Float_Confronta(float *num1, float *num2) {
	if (*num1 == *num2)
		return 0;
	if (*num1 < *num2)
		return -1;
	return 1;
}

float Float_GetKey(float *num) {
	return *num;
}

void Float_Print(float *num) {
	printf("%f\n", *num);
}