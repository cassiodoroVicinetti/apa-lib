#include <stdio.h>
#include <stdlib.h>
#include "int.h"

int Int_Confronta(int *num1, int *num2) {
	return *num1 - *num2;
}

int Int_GetKey(int *num) {
	return *num;
}

void Int_Print(int *num) {
	printf("%d\n", *num);
}