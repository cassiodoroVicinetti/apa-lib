#ifndef _INT_INCLUDED
#define _INT_INCLUDED

int Int_Confronta(int *num1, int *num2);
int Int_GetKey(int *num);
void Int_Print(int *num);

#endif
